[擬似パラメーター](https://docs.aws.amazon.com/ja_jp/AWSCloudFormation/latest/UserGuide/pseudo-parameter-reference.html)などの値を確認したいときに。

[リソース](https://docs.aws.amazon.com/ja_jp/AWSCloudFormation/latest/UserGuide/resources-section-structure.html)が必須で[Outputs](https://docs.aws.amazon.com/ja_jp/AWSCloudFormation/latest/UserGuide/outputs-section-structure.html)だけを指定することはできないので適当なリソースを作成する必要があります。

## Usage

```
$ export AWS_DEFAULT_PROFILE=your_profile
$ STACK_NAME=MyStack
$ aws cloudformation create-stack --stack-name=$STACK_NAME --template-body=file://main.yml
$ aws cloudformation create-change-set --stack-name=$STACK_NAME --template-body=file://main.yml --change-set-name=cs-$(date '+%s')
$ aws cloudformation describe-change-set --stack-name=$STACK_NAME --change-set-name=$(aws cloudformation list-change-sets --stack-name=$STACK_NAME | jq '.Summaries[].ChangeSetName' | tr -d '"')
$ aws cloudformation execute-change-set --stack-name=$STACK_NAME --change-set-name=$(aws cloudformation list-change-sets --stack-name=$STACK_NAME | jq '.Summaries[].ChangeSetName' | tr -d '"')
$ aws cloudformation delete-stack --stack-name=$STACK_NAME
```
