## 概要

AutoScalingグループを使ったFargate検証環境。


## Usage

```
$ export AWS_DEFAULT_PROFILE=your_profile
$ export AWS_DEFAULT_REGION=us-east-1
$ STACK_NAME=MyFargate
$ REPOSITORY_NAME=my_fargate_app
```

### ECR用IAM Userの作成

```
$ aws cloudformation create-stack --stack-name=${STACK_NAME}-iam --template-body=file://iam.yml --capabilities=CAPABILITY_NAMED_IAM
```

### ECRの作成

```
$ aws cloudformation create-stack --stack-name=${STACK_NAME}-ecr --template-body=file://ecr.yml --parameters=ParameterKey=RepositoryName,ParameterValue=${REPOSITORY_NAME}
```
### VPCの作成

```
$ aws cloudformation create-stack --stack-name=${STACK_NAME}-vpc --template-body=file://vpc.yml
```

### SecurityGroupの作成

```
$ aws cloudformation create-stack --stack-name=${STACK_NAME}-sg --template-body=file://sg.yml
```

### LoadBalancerの作成

```
$ aws cloudformation create-stack --stack-name=${STACK_NAME}-lb --template-body=file://lb.yml
```

### ECS Clusterの作成

```
$ aws cloudformation create-stack --stack-name=${STACK_NAME}-ecs-cluster --template-body=file://ecs_cluster.yml
```

### ECS Serviceの作成

```
$ aws cloudformation create-stack --stack-name=${STACK_NAME}-ecs-service --template-body=file://ecs_service.yml
```

