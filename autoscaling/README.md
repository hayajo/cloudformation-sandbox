## 概要

AutoScalingグループを使ったEC2検証環境。

![](img/autoscaling.png)


## Usage

macOSの場合は`base64 -w0 userdata.sh`を`base64 userdata.sh`に変更してください。

```
$ export AWS_DEFAULT_PROFILE=your_profile
$ STACK_NAME=MyStack
$ KEY_NAME=MyKey
$ aws cloudformation create-stack --stack-name=$STACK_NAME --template-body=file://main.yml --parameters "ParameterKey=KeyName,ParameterValue=$KEY_NAME" "ParameterKey=UserData,ParameterValue=$(base64 -w0 userdata.sh)"
$ aws cloudformation create-change-set --stack-name=$STACK_NAME --template-body=file://main.yml --parameters "ParameterKey=KeyName,ParameterValue=$KEY_NAME" "ParameterKey=UserData,ParameterValue=$(base64 -w0 userdata.sh)" --change-set-name=cs-$(date '+%s')
$ aws cloudformation describe-change-set --stack-name=$STACK_NAME --change-set-name=$(aws cloudformation list-change-sets --stack-name=$STACK_NAME | jq '.Summaries[].ChangeSetName' | tr -d '"')
$ aws cloudformation execute-change-set --stack-name=$STACK_NAME --change-set-name=$(aws cloudformation list-change-sets --stack-name=$STACK_NAME | jq '.Summaries[].ChangeSetName' | tr -d '"')
$ aws cloudformation delete-stack --stack-name=$STACK_NAME
```
